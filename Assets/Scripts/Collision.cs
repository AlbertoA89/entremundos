﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Collision : MonoBehaviour {
    
    public static int baus = 0;
    bool flagBau1 = true;
    bool flagBau2 = true;

    void Start()
    {
        Collision.baus = 0;
        GameObject.Find("bauBG").GetComponent<Image>().color = Color.white;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.name == "bau" && flagBau1)
        {
            GameObject.Find("success").GetComponent<SpriteRenderer>().enabled = true;
            GameObject.Find("pointer").GetComponent<Animator>().enabled = true;
            GameObject.Find("bau2").GetComponent<SpriteRenderer>().enabled = true;
            GameObject.Find("bau2").GetComponent<BoxCollider2D>().enabled = true;
            
            baus++;
            flagBau1 = false;
            GameObject.Find("baus").GetComponent<Text>().text = baus.ToString();
        }
        if (coll.gameObject.name == "bau2" && flagBau2)
        {
            GameObject.Find("success2").GetComponent<SpriteRenderer>().enabled = true;
            baus++;
            flagBau2 = false;
            GameObject.Find("baus").GetComponent<Text>().text = baus.ToString();
        }
    }

}
