﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Gameplay : MonoBehaviour {

    public GameObject bullet;

    void Start()
    {
        Collision.baus = 0;
        GameObject.Find("bauBG").GetComponent<Image>().color = Color.white;
        GameObject.Find("skybox").GetComponent<SpriteRenderer>().color = new Color32(7, 86, 195, 255);
        GameObject.Find("hehe").GetComponent<SpriteRenderer>().color = new Color32(7, 86, 195, 255);
    }
    
    void alives()
    {
        showWorld("Alives", true);
        showWorld("Spirits", false);
        showWorld("Deads", false);
        GameObject.Find("skybox").GetComponent<SpriteRenderer>().color = new Color32(7, 86, 195, 255);
        GameObject.Find("hehe").GetComponent<SpriteRenderer>().color = new Color32(7, 86, 195, 255);
    }

    void spirits()
    {
        showWorld("Alives", false);
        showWorld("Spirits", true);
        showWorld("Deads", false);
        GameObject.Find("skybox").GetComponent<SpriteRenderer>().color = new Color32(179, 101, 191, 255);
        GameObject.Find("hehe").GetComponent<SpriteRenderer>().color = new Color32(179, 101, 191, 255);
    }

    void deads()
    {
        showWorld("Alives", false);
        showWorld("Spirits", false);
        showWorld("Deads", true);
        GameObject.Find("skybox").GetComponent<SpriteRenderer>().color = new Color32(191, 101, 101, 255);
        GameObject.Find("hehe").GetComponent<SpriteRenderer>().color = new Color32(191, 101, 101, 255);
    }

    void showWorld(string world, bool show)
    {
        GameObject[] worldObjs = GameObject.FindGameObjectsWithTag(world);
        foreach (GameObject objs in worldObjs)
        {
            objs.GetComponent<SpriteRenderer>().enabled = show;
            objs.GetComponent<BoxCollider2D>().enabled = show;
        }
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1))
        {
            alives();
        }

        if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2))
        {
            spirits();
        }
            
        if (Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Keypad3))
        {
            deads();
        }
        if(Collision.baus == 2)
        {
            GameObject.Find("bauBG").GetComponent<Image>().color = Color.green;
            GameObject.Find("finalchest").GetComponent<Animator>().enabled = true;
        }
    }
}
